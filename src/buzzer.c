#include "buzzer.h"
#include "sys/devices.h"
#include "sys/clock.h"

void init_buzzer () {
    
    // Config PB9
    GPIOB.MODER = ( GPIOB.MODER & ~(3 << 18) ) | (2 << 18);
    GPIOB.AFRH  = ( GPIOB.AFRH  & ~(0xf << 4) ) | (1 << 4);

    enable_TIM2();
    
    // Désactivation slave mode
    TIM2.SMCR &= ~( (1 << 14) | (1 << 7) | 7 );
    
    // Désactivation sortie CH2
    TIM2.CCER &= ~(1 << 4);

    // Configuration CH2
    TIM2.CCMR1 = (TIM2.CCMR1 & ~(0xFF << 8)) | (0x68 << 8);

    // Activation sortie CH2
    TIM2.CCER |= (1 << 4);

    // Configuration PSC
    TIM2.PSC = (2 * get_APB1CLK() / MAX_FREQ);

    // Activation de l'interruption en cas d'UEV
    // TIM2.DIER |= 1;

    // Reinitialisation compteur
    TIM2.ARR = 0;
    TIM2.CCR2 = 0;
    TIM2.CNT = 0;

    // Activation CEN et ARPE dans CR1
    TIM2.CR1 = (TIM2.CR1 & ~(0x3FF)) | 0x81;

    // Activation Interrupt NVIC
    // NVIC.ISER[28/32] = (1<<(28%32));

}

void force_UEV () {
    TIM2.CNT = 0;
    TIM2.EGR |= 1;
}

void set_frequence (uint32_t frequence) {
    TIM2.ARR = frequence ? MAX_FREQ / frequence : MAX_FREQ / 20000;
}

void set_volume (uint32_t volume) {
    TIM2.CCR2 = ((volume >> 1) * TIM2.ARR) / 4096;
}