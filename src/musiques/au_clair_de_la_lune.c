#include <stdint.h>
#include "notes.h"
#include "au_clair_de_la_lune.h"

/* Place occupée en mémoire: 2,1 ko */

const uint32_t au_clair_de_la_lune__piste0[272] = {
    /* Mesure 1 */ DO4, DO4, DO4, 0, DO4, DO4, DO4, 0, DO4, DO4, DO4, 0, RE4, RE4, RE4, 0,
    /* Mesure 2 */ MI4, MI4, MI4, MI4, MI4, MI4, MI4, 0, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0,
    /* Mesure 3 */ DO4, DO4, DO4, 0, MI4, MI4, MI4, 0, RE4, RE4, RE4, 0, RE4, RE4, RE4, 0,
    /* Mesure 4 */ DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, 0,
    /* Mesure 5 */ DO4, DO4, DO4, 0, DO4, DO4, DO4, 0, DO4, DO4, DO4, 0, RE4, RE4, RE4, 0,
    /* Mesure 6 */ MI4, MI4, MI4, MI4, MI4, MI4, MI4, 0, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0,
    /* Mesure 7 */ DO4, DO4, DO4, 0, MI4, MI4, MI4, 0, RE4, RE4, RE4, 0, RE4, RE4, RE4, 0,
    /* Mesure 8 */ DO4, DO4, DO4, DO4, DO4, DO4, DO4, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 9 */ RE4, RE4, RE4, 0, RE4, RE4, RE4, 0, RE4, RE4, RE4, 0, RE4, RE4, RE4, 0,
    /* Mesure 10 */ LA3, LA3, LA3, LA3, LA3, LA3, LA3, 0, LA3, LA3, LA3, LA3, LA3, LA3, LA3, 0,
    /* Mesure 11 */ RE4, RE4, RE4, 0, DO4, DO4, DO4, 0, SI3, SI3, SI3, 0, LA3, LA3, LA3, 0,
    /* Mesure 12 */ SOL3, SOL3, SOL3, SOL3, SOL3, SOL3, SOL3, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 13 */ DO4, DO4, DO4, 0, DO4, DO4, DO4, 0, DO4, DO4, DO4, 0, RE4, RE4, RE4, 0,
    /* Mesure 14 */ MI4, MI4, MI4, MI4, MI4, MI4, MI4, 0, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0,
    /* Mesure 15 */ DO4, DO4, DO4, 0, MI4, MI4, MI4, 0, RE4, RE4, RE4, 0, RE4, RE4, RE4, 0,
    /* Mesure 16 */ DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, DO4, 0,
    /* Mesure 17 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
const uint32_t au_clair_de_la_lune__piste1[272] = {
    /* Mesure 1 */ DO3, DO3, DO3, 0, SOL3, SOL3, SOL3, 0, MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0,
    /* Mesure 2 */ DO3, DO3, DO3, 0, SOL3, SOL3, SOL3, 0, MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0,
    /* Mesure 3 */ MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0, FA3, FA3, FA3, FA3, FA3, FA3, FA3, 0,
    /* Mesure 4 */ MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0, MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0,
    /* Mesure 5 */ DO3, DO3, DO3, 0, SOL3, SOL3, SOL3, 0, MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0,
    /* Mesure 6 */ DO3, DO3, DO3, 0, SOL3, SOL3, SOL3, 0, MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0,
    /* Mesure 7 */ MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0, FA3, FA3, FA3, FA3, FA3, FA3, FA3, 0,
    /* Mesure 8 */ MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0, MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0,
    /* Mesure 9 */ SI2, SI2, SI2, SI2, SI2, SI2, SI2, 0, SOL3, SOL3, SOL3, SOL3, SOL3, SOL3, SOL3, 0,
    /* Mesure 10 */ DO3, DO3, DO3, 0, SOL3, SOL3, SOL3, 0, DO3, DO3, DO3, 0, SOL3, SOL3, SOL3, 0,
    /* Mesure 11 */ RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, RE3, 0,
    /* Mesure 12 */ SOL3, SOL3, SOL3, 0, FA3, FA3, FA3, 0, MI3, MI3, MI3, 0, RE3, RE3, RE3, 0,
    /* Mesure 13 */ DO3, DO3, DO3, 0, DO3, DO3, DO3, 0, DO3, DO3, DO3, 0, RE3, RE3, RE3, 0,
    /* Mesure 14 */ MI3, MI3, MI3, MI3, MI3, MI3, MI3, 0, RE3, RE3, RE3, RE3, RE3, RE3, RE3, 0,
    /* Mesure 15 */ MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0, FA3, FA3, FA3, FA3, FA3, FA3, FA3, 0,
    /* Mesure 16 */ MI3, MI3, MI3, 0, SOL3, SOL3, SOL3, 0, DO3, DO3, DO3, DO3, DO3, DO3, DO3, 0,
    /* Mesure 17 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};

musique au_clair_de_la_lune () {
    musique m;

    m.BPM = 94 * 1.5;
    m.div_tempo = 4;
    m.nb_notes_piste = 264;
    m.nb_pistes = 2;
    m.pistes[0] = au_clair_de_la_lune__piste0;
    m.pistes[1] = au_clair_de_la_lune__piste1;
    m.pistes[2] = au_clair_de_la_lune__piste0;
    m.pistes[3] = au_clair_de_la_lune__piste0;
    m.pistes[4] = au_clair_de_la_lune__piste0;
    m.pistes[5] = au_clair_de_la_lune__piste0;
    m.pistes[6] = au_clair_de_la_lune__piste0;
    m.pistes[7] = au_clair_de_la_lune__piste0;

    return m;
}