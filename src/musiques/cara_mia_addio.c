#include <stdint.h>
#include "../notes.h"
#include "cara_mia_addio.h"

/* Place occupée en mémoire: 4,7 ko */

const uint32_t cara_mia_addio__piste0[592] = {
    /* Mesure 1 */ LA4, LA4, 0, 0, LAd4, LAd4, 0, 0, DO5, DO5, 0, 0, LAd4, LAd4, LA4, LA4,
    /* Mesure 2 */ RE5, RE5, DO5, DO5, LAd4, LAd4, 0, 0, LA4, LA4, LA4, LA4, LAd4, LAd4, DO5, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 3 */ RE5, RE5, RE5, RE5, DO5, DO5, DO5, DO5, 0, 0, 0, 0, LAd4, LAd4, 0, 0,
    /* Mesure 4 */ DO5, DO5, DO5, DO5, 0, 0, 0, 0, LAd4, LAd4, DO5, DO5, RE5, RE5, RE5, RE5,
    /* Mesure 5 */ DO5, DO5, LAd4, LAd4, LAd4, LAd4, 0, 0, LAd4, LAd4, LAd4, LAd4, DO5, DO5, LAd4, LAd4,
    /* Mesure 6 */ LA4, LA4, LAd4, LAd4, DO5, DO5, 0, 0, REd5, REd5, REd5, REd5, RE5, RE5, DO5, DO5,
    /* Mesure 7 */ RE5, RE5, RE5, RE5, DO5, DO5, 0, 0, LAd4, LAd4, LAd4, LAd4, DO5, DO5, DO5, DO5,
    /* Mesure 8 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, RE5, RE5, DO5, DO5, LAd4, LAd4, DO5, DO5,
    /* Mesure 9 */ LAd4, DO5, DO5, DO5, LAd4, LAd4, 0, 0, LAd4, LAd4, LAd4, LAd4, DO5, DO5, LAd4, LAd4,
    /* Mesure 10 */ LA4, LA4, LAd4, LAd4, DO5, DO5, DO5, DO5, RE5, RE5, RE5, RE5, DO5, DO5, DO5, DO5,
    /* Mesure 11 */ LAd4, LAd4, 0, 0, LAd4, LAd4, LAd4, LAd4, DO5, DO5, 0, 0, RE5, RE5, RE5, RE5,
    /* Mesure 12 */ DO5, DO5, LAd4, LAd4, DO5, DO5, DO5, RE5, 0, REd5, 0, 0, REd5, REd5, REd5, REd5,
    /* Mesure 13 */ RE5, RE5, RE5, RE5, DO5, DO5, DO5, DO5, LAd4, LAd4, DO5, DO5, DO5|UEV, DO5, DO5, DO5,
    /* Mesure 14 */ DO5, DO5, DO5, DO5, LAd4, LAd4, LAd4, LAd4, LA4, LA4, LA4, LA4, LAd4, LAd4, LAd4, LAd4,
    /* Mesure 15 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 16 */ LA4, LA4, LA4, LA4, LAd4, LAd4, LAd4, LAd4, DO5, DO5, DO5, DO5, LAd4, LAd4, LA4, LA4,
    /* Mesure 17 */ RE5, RE5, DO5, DO5, LAd4, LAd4, LAd4, LAd4, LA4, LA4, LA4, LA4, LAd4, LAd4, SI4, SI4,
    /* Mesure 18 */ RE5, RE5, DO5, DO5, DO5|UEV, DO5, DO5, DO5, 0, 0, 0, 0, LAd4, LAd4, LAd4, LAd4,
    /* Mesure 19 */ DO5, DO5, DO5, DO5, 0, 0, 0, 0, LAd4, LAd4, DO5, DO5, DO5, DO5, DO5, DO5,
    /* Mesure 20 */ DO5, DO5, LAd4, LAd4, LAd4|UEV, LAd4, LAd4, LAd4, LAd4|UEV, LAd4, LAd4, DO5, DO5|UEV, DO5, SOLd4, SOLd4,
    /* Mesure 21 */ LAd4, LAd4, LAd4, LAd4, DO5, DO5, DO5, DO5, REd5, REd5, RE5, RE5, RE5|UEV, RE5, DO5, DO5,
    /* Mesure 22 */ RE5, RE5, RE5, RE5, DO5, DO5, DO5, DO5, LAd4, LAd4, LA4, LA4, LAd4, LAd4, DO5, DO5,
    /* Mesure 23 */ DO5, DO5, DO5, DO5, 0, 0, 0, 0, RE5, RE5, DO5, DO5, DO5|UEV, DO5, DO5, DO5,
    /* Mesure 24 */ LAd4, LAd4, DO5, DO5, LAd4, LAd4, LAd4, LAd4, LA4, LA4, LA4, LA4, LAd4, LAd4, LAd4, LAd4,
    /* Mesure 25 */ FA5, FA5, FA5, FA5, REd5, REd5, RE5, RE5, DO5, DO5, LAd4, LAd4, DO5, DO5, RE5, RE5,
    /* Mesure 26 */ RE5|UEV, RE5, RE5, RE5, DO5, DO5, DO5, DO5, 0, 0, 0, 0, RE5, RE5, REd5, REd5,
    /* Mesure 27 */ REd5|UEV, REd5, REd5, REd5, RE5, RE5, RE5, RE5, DO5, DO5, DO5, DO5, RE5, RE5, RE5, RE5,
    /* Mesure 28 */ DO5, DO5, DO5, DO5, LAd4, LAd4, LAd4, LAd4, LAd4|UEV, LAd4, LAd4, LAd4, DO5, DO5, DO5, DO5,
    /* Mesure 29 */ RE5, RE5, RE5, RE5, DO5, DO5, LAd4, LAd4, DO5, DO5, DO5, RE5, RE5, REd5, REd5, REd5,
    /* Mesure 30 */ REd5|UEV, REd5, REd5, REd5, RE5, RE5, RE5, RE5, DO5, DO5, DO5, DO5, LAd4, LAd4, DO5, DO5,
    /* Mesure 31 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, LAd4, LAd4, LAd4, LAd4, LA4, LA4, LA4, LA4,
    /* Mesure 32 */ LAd4, LAd4, LAd4, LAd4, DO5, DO5, DO5, DO5, RE5, RE5, RE5, RE5, DO5, DO5, LAd4, LAd4,
    /* Mesure 33 */ DO5, DO5, LAd4, LAd4, LA4, LA4, LA4, LA4, LAd4, LAd4, LAd4, LAd4, LAd4|UEV, LAd4, DO5, DO5,
    /* Mesure 34 */ RE5, RE5, RE5, RE5, DO5, DO5, DO5, DO5, 0, 0, 0, 0, REd5, REd5, REd5, REd5,
    /* Mesure 35 */ REd5|UEV, REd5, REd5, REd5, RE5, RE5, RE5, RE5, DO5, DO5, LAd4, LAd4, DO5, DO5, DO5|UEV, DO5,
    /* Mesure 36 */ RE5, RE5, RE5, RE5, RE5, RE5, RE5, RE5, RE5, RE5, RE5, RE5, 0, 0, 0, 0,
    /* Mesure 37 */ 0, 0, 0, 0, 0, 0, 0, 0
};
const uint32_t cara_mia_addio__piste1[592] = {
    /* Mesure 1 */ LAd3, LAd3, 0, 0, SI4, SI4, 0, 0, LAd3, LAd3, 0, 0, SI4, SI4, 0, 0,
    /* Mesure 2 */ LAd3, LAd3, 0, 0, SI4, SI4, 0, 0, LAd3, LAd3, 0, 0, SI4, SI4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 3 */ LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0, LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0,
    /* Mesure 4 */ LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0, LAd3, LAd3, 0, 0, LA3, LA3, LA3, LA3,
    /* Mesure 5 */ LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0, LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0,
    /* Mesure 6 */ LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0, LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0,
    /* Mesure 7 */ DO4, DO4, 0, 0, SI4, SI4, 0, 0, DO4, DO4, 0, 0, SI4, SI4, 0, 0,
    /* Mesure 8 */ DO4, DO4, 0, 0, SI4, SI4, 0, 0, DO4, DO4, 0, 0, SI4, SI4, 0, 0,
    /* Mesure 9 */ LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0, LAd3, LAd3, 0, 0, SOL4, SOL4, 0, 0,
    /* Mesure 10 */ LA4, LA4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 11 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 12 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 13 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 14 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 15 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 16 */ LAd3, LAd3, FA4, FA4, FA4|UEV, FA4, FA4, FA4, LA3, LA3, FA4, FA4, FA4|UEV, FA4, FA4, FA4,
    /* Mesure 17 */ SOL3, SOL3, RE4, RE4, RE4|UEV, RE4, RE4, RE4, SOL3, SOL3, RE4, RE4, RE4|UEV, RE4, RE4, RE4,
    /* Mesure 18 */ REd3, REd3, REd4, REd4, REd4|UEV, REd4, REd4, REd4, REd3, REd3, REd4, REd4, REd4|UEV, REd4, REd4, REd4,
    /* Mesure 19 */ FA3, FA3, DO4, DO4, DO4|UEV, DO4, DO4, DO4, LA3, LA3, DO4, DO4, DO4|UEV, DO4, DO4, DO4,
    /* Mesure 20 */ LAd3, LAd3, RE4, RE4, RE4|UEV, RE4, RE4, RE4, LAd3, LAd3, RE4, RE4, RE4|UEV, RE4, RE4, RE4,
    /* Mesure 21 */ LA3, LA3, DO4, DO4, DO4|UEV, DO4, DO4, DO4, FA3, FA3, DO4, DO4, DO4|UEV, DO4, DO4, DO4,
    /* Mesure 22 */ REd3, REd3, LAd3, REd4, REd4, REd4, SOL4, SOL4, REd4, REd4, REd4, LAd3, LAd3, LAd3, REd4, REd4,
    /* Mesure 23 */ REd3, REd3, FA4, FA4, FA4|UEV, FA4, FA4, FA4, FA3, FA3, FA4, FA4, FA4|UEV, FA4, FA4, FA4,
    /* Mesure 24 */ LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, DO3|UEV, DO3, DO3, DO3, DO3, DO3, DO3, DO3,
    /* Mesure 25 */ LA2, LA2, LA2, LA2, LA2, LA2, LA2, LA2, LA2|UEV, LA2, LA2, LA2, LA2, LA2, LA2, LA2,
    /* Mesure 26 */ SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2|UEV, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2,
    /* Mesure 27 */ REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2|UEV, REd2, REd2, REd2, REd2, REd2, REd2, REd2,
    /* Mesure 28 */ LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2|UEV, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2,
    /* Mesure 29 */ LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2|UEV, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2,
    /* Mesure 30 */ REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2|UEV, REd2, REd2, REd2, REd2, REd2, REd2, REd2,
    /* Mesure 31 */ REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2|UEV, REd2, REd2, REd2, REd2, REd2, REd2, REd2,
    /* Mesure 32 */ LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LA2, LA2, LA2, LA2, LA2, LA2, LA2, LA2,
    /* Mesure 33 */ SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2|UEV, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2, SOL2,
    /* Mesure 34 */ REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2, REd2|UEV, REd2, REd2, REd2, REd2, REd2, REd2, REd2,
    /* Mesure 35 */ FA2, FA2, FA2, FA2, FA2, FA2, FA2, FA2, LA2, LA2, LA2, LA2, LA2, LA2, LA2, LA2,
    /* Mesure 36 */ LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, 0, 0, 0, 0,
    /* Mesure 37 */ 0, 0, 0, 0, 0, 0, 0, 0
};

musique cara_mia_addio () {
    musique m;

    m.BPM = 95;
    m.div_tempo = 4;
    m.nb_notes_piste = 592;
    m.nb_pistes = 2;
    m.pistes[0] = cara_mia_addio__piste0;
    m.pistes[1] = cara_mia_addio__piste1;
    m.pistes[2] = cara_mia_addio__piste0;
    m.pistes[3] = cara_mia_addio__piste0;
    m.pistes[4] = cara_mia_addio__piste0;
    m.pistes[5] = cara_mia_addio__piste0;
    m.pistes[6] = cara_mia_addio__piste0;
    m.pistes[7] = cara_mia_addio__piste0;

    return m;
}