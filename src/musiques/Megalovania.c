#include <stdint.h>
#include "../notes.h"
#include "Megalovania.h"

/* Place occupée en mémoire: 3,1 ko */

const uint32_t Megalovania__piste0[256] = {
    /* Mesure 1 */ RE4, RE4|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 2 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 3 */ DO4, DO4|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 4 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 5 */ SI3, SI3|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 6 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 7 */ LAd3, LAd3|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 8 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 9 */ RE4, RE4|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 10 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 11 */ DO4, DO4|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 12 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 13 */ SI3, SI3|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 14 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 15 */ LAd3, LAd3|UEV, RE5, RE5, LA4, LA4, LA4, SOLd4,
    /* Mesure 16 */ SOLd4, SOL4, SOL4, FA4, FA4, RE4, FA4, SOL4,
    /* Mesure 17 */ RE5, RE5|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 18 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5,
    /* Mesure 19 */ DO5, DO5|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 20 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5,
    /* Mesure 21 */ SI4, SI4|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 22 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5,
    /* Mesure 23 */ LAd4, LAd4|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 24 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5,
    /* Mesure 25 */ RE5, RE5|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 26 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5,
    /* Mesure 27 */ DO5, DO5|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 28 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5,
    /* Mesure 29 */ SI4, SI4|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 30 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5,
    /* Mesure 31 */ LAd4, LAd4|UEV, RE6, RE6, LA5, LA5, LA5, SOLd5,
    /* Mesure 32 */ SOLd5, SOL5, SOL5, FA5, FA5, RE5, FA5, SOL5
};
const uint32_t Megalovania__piste1[256] = {
    /* Mesure 1 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 2 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 3 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 4 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 5 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 6 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 7 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 8 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 9 */ RE3, RE3, RE3|UEV, RE3, RE3|UEV, RE3|UEV, RE3, RE3|UEV,
    /* Mesure 10 */ RE3, RE3|UEV, RE3, RE3|UEV, RE3, RE3|UEV, RE3|UEV, RE3,
    /* Mesure 11 */ DO3, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3, DO3|UEV,
    /* Mesure 12 */ DO3, DO3|UEV, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3,
    /* Mesure 13 */ SI2, SI2, SI2|UEV, SI2, SI2|UEV, SI2|UEV, SI2, SI2|UEV,
    /* Mesure 14 */ SI2, SI2|UEV, SI2, SI2|UEV, SI2, SI2|UEV, SI2|UEV, SI2,
    /* Mesure 15 */ LAd2, LAd2, LAd2|UEV, LAd2, LAd2|UEV, LAd2|UEV, LAd2, DO3,
    /* Mesure 16 */ DO3, DO3|UEV, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3,
    /* Mesure 17 */ RE3, RE3, RE3|UEV, RE3, RE3|UEV, RE3|UEV, RE3, RE3|UEV,
    /* Mesure 18 */ RE3, RE3|UEV, RE3, RE3|UEV, RE3, RE3|UEV, RE3|UEV, RE3,
    /* Mesure 19 */ DO3, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3, DO3|UEV,
    /* Mesure 20 */ DO3, DO3|UEV, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3,
    /* Mesure 21 */ SI2, SI2, SI2|UEV, SI2, SI2|UEV, SI2|UEV, SI2, SI2|UEV,
    /* Mesure 22 */ SI2, SI2|UEV, SI2, SI2|UEV, SI2, SI2|UEV, SI2|UEV, SI2,
    /* Mesure 23 */ LAd2, LAd2, LAd2|UEV, LAd2, LAd2|UEV, LAd2|UEV, LAd2, DO3,
    /* Mesure 24 */ DO3, DO3|UEV, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3,
    /* Mesure 25 */ RE3, RE3, RE3|UEV, RE3, RE3|UEV, RE3|UEV, RE3, RE3|UEV,
    /* Mesure 26 */ RE3, RE3|UEV, RE3, RE3|UEV, RE3, RE3|UEV, RE3|UEV, RE3,
    /* Mesure 27 */ DO3, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3, DO3|UEV,
    /* Mesure 28 */ DO3, DO3|UEV, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3,
    /* Mesure 29 */ SI2, SI2, SI2|UEV, SI2, SI2|UEV, SI2|UEV, SI2, SI2|UEV,
    /* Mesure 30 */ SI2, SI2|UEV, SI2, SI2|UEV, SI2, SI2|UEV, SI2|UEV, SI2,
    /* Mesure 31 */ LAd2, LAd2, LAd2|UEV, LAd2, LAd2|UEV, LAd2|UEV, LAd2, DO3,
    /* Mesure 32 */ DO3, DO3|UEV, DO3, DO3|UEV, DO3, DO3|UEV, DO3|UEV, DO3
};
const uint32_t Megalovania__piste2[256] = {
    /* Mesure 1 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 2 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 3 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 4 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 5 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 6 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 7 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 8 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 9 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 10 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 11 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 12 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 13 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 14 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 15 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 16 */ 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 17 */ RE2, RE2, RE2|UEV, RE2, RE2|UEV, RE2|UEV, RE2, RE2|UEV,
    /* Mesure 18 */ RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2|UEV, RE2,
    /* Mesure 19 */ DO2, DO2, DO2|UEV, DO2, DO2|UEV, DO2|UEV, DO2, DO2|UEV,
    /* Mesure 20 */ DO2, DO2|UEV, DO2, DO2|UEV, DO2, DO2|UEV, DO2|UEV, DO2,
    /* Mesure 21 */ SI1, SI1, SI1|UEV, SI1, SI1|UEV, SI1|UEV, SI1, SI1|UEV,
    /* Mesure 22 */ SI1, SI1|UEV, SI1, SI1|UEV, SI1, SI1|UEV, SI1|UEV, SI1,
    /* Mesure 23 */ LAd1, LAd1, LAd1|UEV, LAd1, LAd1|UEV, LAd1|UEV, LAd1, LAd1,
    /* Mesure 24 */ LAd1, LAd1|UEV, LAd1, LAd1|UEV, LAd1, LAd1|UEV, LAd1|UEV, LAd1,
    /* Mesure 25 */ RE2, RE2, RE2|UEV, RE2, RE2|UEV, RE2|UEV, RE2, RE2|UEV,
    /* Mesure 26 */ RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2|UEV, RE2,
    /* Mesure 27 */ DO2, DO2, DO2|UEV, DO2, DO2|UEV, DO2|UEV, DO2, DO2|UEV,
    /* Mesure 28 */ DO2, DO2|UEV, DO2, DO2|UEV, DO2, DO2|UEV, DO2|UEV, DO2,
    /* Mesure 29 */ SI1, SI1, SI1|UEV, SI1, SI1|UEV, SI1|UEV, SI1, SI1|UEV,
    /* Mesure 30 */ SI1, SI1|UEV, SI1, SI1|UEV, SI1, SI1|UEV, SI1|UEV, SI1,
    /* Mesure 31 */ LAd1, LAd1, LAd1|UEV, LAd1, LAd1|UEV, LAd1|UEV, LAd1, LAd1,
    /* Mesure 32 */ LAd1, LAd1|UEV, LAd1, LAd1|UEV, LAd1, LAd1|UEV, LAd1|UEV, LAd1
};

musique Megalovania () {
    musique m;

    m.BPM = 120;
    m.div_tempo = 4;
    m.nb_notes_piste = 256;
    m.nb_pistes = 3;
    m.pistes[0] = Megalovania__piste0;
    m.pistes[1] = Megalovania__piste1;
    m.pistes[2] = Megalovania__piste2;
    m.pistes[3] = Megalovania__piste0;
    m.pistes[4] = Megalovania__piste0;
    m.pistes[5] = Megalovania__piste0;
    m.pistes[6] = Megalovania__piste0;
    m.pistes[7] = Megalovania__piste0;

    return m;
}

