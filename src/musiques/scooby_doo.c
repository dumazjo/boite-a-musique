#include <stdint.h>
#include "notes.h"
#include "scooby_doo.h"

/* Place occupée en mémoire: 8,7 ko */

const uint32_t scooby_doo__piste0[544] = {
    /* Mesure 1 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 2 */ RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, RE4, RE4, RE4, RE4,
    /* Mesure 3 */ DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, 0, 0, 0, 0, 0, 0, 0, 0, RE4, RE4, RE4, RE4, MI4, MI4, MI4, MI4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 4 */ FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4|UEV, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, SI3, SI3, SI3, SI3,
    /* Mesure 5 */ SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 6 */ RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, RE4, RE4, RE4, RE4,
    /* Mesure 7 */ DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 8 */ RE4, RE4, RE4, RE4, RE4|UEV, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 9 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 10 */ RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, RE4, RE4, RE4, RE4,
    /* Mesure 11 */ DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, 0, 0, 0, 0, 0, 0, 0, 0, RE4, RE4, RE4, RE4, MI4, MI4, MI4, MI4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 12 */ FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4|UEV, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, SOLd3, SOLd3, SOLd3, SOLd3,
    /* Mesure 13 */ SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 14 */ RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, RE4, RE4, RE4, RE4,
    /* Mesure 15 */ DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, DOd4, 0, 0, 0, 0, 0, 0, 0, 0, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4, MI4, MI4, MI4, MI4, FAd4, FAd4, FAd4, FAd4,
    /* Mesure 16 */ RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4|UEV, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4, RE4,
    /* Mesure 17 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
};
const uint32_t scooby_doo__piste1[544] = {
    /* Mesure 1 */ DO2, DO2, DO2, DO2, 0, 0, 0, 0, RE2, RE2, RE2, RE2, 0, 0, 0, 0, RE2, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2,
    /* Mesure 2 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 3 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 4 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 5 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 6 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 7 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 8 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 9 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, DO2, DO2, DO2, DO2, RE2, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2,
    /* Mesure 10 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 11 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 12 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 13 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 14 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 15 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, RE2, RE2, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 16 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0, DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, 0, 0, 0, 0,
    /* Mesure 17 */ DO2, DO2, DO2, DO2, DO2|UEV, DO2, DO2, DO2, RE2, RE2, RE2, RE2, DO2, DO2, DO2, DO2, RE2, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2
};
const uint32_t scooby_doo__piste2[544] = {
    /* Mesure 1 */ 0, 0, 0, 0, 0, 0, 0, 0, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2, LAd2,
    /* Mesure 2 */ SOL1, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1,
    /* Mesure 3 */ LA1, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1,
    /* Mesure 4 */ RE2, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2,
    /* Mesure 5 */ MI2, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2,
    /* Mesure 6 */ SOL1, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1,
    /* Mesure 7 */ LA1, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1,
    /* Mesure 8 */ RE2, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2,
    /* Mesure 9 */ RE2, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2,
    /* Mesure 10 */ SOL1, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1,
    /* Mesure 11 */ LA1, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1,
    /* Mesure 12 */ RE2, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2,
    /* Mesure 13 */ MI2, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2, MI2|UEV, MI2, MI2, MI2,
    /* Mesure 14 */ SOL1, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1, SOL1|UEV, SOL1, SOL1, SOL1,
    /* Mesure 15 */ LA1, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1, LA1|UEV, LA1, LA1, LA1,
    /* Mesure 16 */ RE2, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2,
    /* Mesure 17 */ RE2, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, RE2|UEV, RE2, RE2, RE2, LA2, LA2, LA2, LA2, LA2, LA2, LA2, LA2
};
const uint32_t scooby_doo__piste3[544] = {
    /* Mesure 1 */ 0, 0, 0, 0, 0, 0, 0, 0, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4,
    /* Mesure 2 */ RE3, RE3, RE3, RE3, RE3|UEV, RE3, RE3, RE3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 3 */ MI3, MI3, MI3, MI3, MI3|UEV, MI3, MI3, MI3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 4 */ LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 5 */ SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, 0, 0, 0, 0, 0, 0, 0, 0, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 6 */ RE3, RE3, RE3, RE3, RE3|UEV, RE3, RE3, RE3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 7 */ MI3, MI3, MI3, MI3, MI3|UEV, MI3, MI3, MI3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 8 */ LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, LA3, LA3, LA3, LA3,
    /* Mesure 9 */ SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, SI3, SI3, SI3, SI3,
    /* Mesure 10 */ RE3, RE3, RE3, RE3, RE3|UEV, RE3, RE3, RE3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 11 */ MI3, MI3, MI3, MI3, MI3|UEV, MI3, MI3, MI3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 12 */ LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 13 */ SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, 0, 0, 0, 0, 0, 0, 0, 0, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 14 */ RE3, RE3, RE3, RE3, RE3|UEV, RE3, RE3, RE3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 15 */ MI3, MI3, MI3, MI3, MI3|UEV, MI3, MI3, MI3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 16 */ LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, LA3, LA3, LA3, LA3, LA3|UEV, LA3, LA3, LA3, LA3, LA3, LA3, LA3,
    /* Mesure 17 */ SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, SI3, SI3, SI3, SI3, SI3|UEV, SI3, SI3, SI3, SI3, SI3, SI3, SI3
};

musique scooby_doo () {
    musique m;

    m.BPM = 138 * 1.3;
    m.div_tempo = 8;
    m.nb_notes_piste = 544;
    m.nb_pistes = 4;
    m.pistes[0] = scooby_doo__piste0;
    m.pistes[3] = scooby_doo__piste1;
    m.pistes[2] = scooby_doo__piste2;
    m.pistes[1] = scooby_doo__piste3;
    m.pistes[4] = scooby_doo__piste0;
    m.pistes[5] = scooby_doo__piste0;
    m.pistes[6] = scooby_doo__piste0;
    m.pistes[7] = scooby_doo__piste0;

    return m;
}