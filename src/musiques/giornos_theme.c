#include <stdint.h>
#include "notes.h"
#include "giornos_theme.h"

/* Place occupée en mémoire: 5,1 ko */

const uint32_t giornos_theme__piste0[640] = {
    /* Mesure 1 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 2 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, FA2, FA2, FA2, FA2, MI2, MI2, MI2, MI2, RE2, RE2, LA1, LA1, LA1, LA1,
    /* Mesure 3 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 4 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, SI1, SI1, RE2, RE2, RE2, RE2, FA2, FA2, FA2, FA2, MI2, MI2, MI2, MI2, RE2, RE2, LA1, LA1, LA1, LA1,
    /* Mesure 5 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 6 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, FA2, FA2, FA2, FA2, MI2, MI2, MI2, MI2, RE2, RE2, LA1, LA1, LA1, LA1,
    /* Mesure 7 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 8 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, SI1, SI1, SI1, SI1, LA1, LA1, SI1, SI1, SI1, SI1, SI1, SI1, SI1, SI1, SI1, SI1, SI1, SI1, 0, 0, 0, 0, 0, 0,
    /* Mesure 9 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 10 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, FA2, FA2, FA2, FA2, MI2, MI2, MI2, MI2, RE2, RE2, LA1, LA1, LA1, LA1,
    /* Mesure 11 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 12 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, SI1, SI1, RE2, RE2, RE2, RE2, FA2, FA2, FA2, FA2, MI2, MI2, MI2, MI2, RE2, RE2, LA1, LA1, LA1, LA1,
    /* Mesure 13 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 14 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, FA2, FA2, FA2, FA2, MI2, MI2, MI2, MI2, RE2, RE2, LA1, LA1, LA1, LA1,
    /* Mesure 15 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, SI1, SI1, SI1|UEV, SI1, LA1, LA1, LA1, LA1, SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, SI1, SI1, SI1, SI1, FAd1, FAd1, LA1, LA1, LA1, LA1,
    /* Mesure 16 */ SI1, SI1, SI1, SI1, SI1|UEV, SI1, 0, 0, SI1, SI1, SI1, SI1, LA1, LA1, SI1, SI1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 17 */ SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, MI2, MI2, FA2, FA2, FA2, FA2, FAd2, FAd2, FAd2, FAd2, LA2, LA2, LA2, LA2, FAd2, FAd2, FAd2, FAd2, LA2, LA2, LAd2, LAd2, LAd2, LAd2,
    /* Mesure 18 */ SI2, SI2, SI2, SI2, LAd2, LAd2, LAd2, LAd2, LA2, LA2, FAd2, FAd2, FAd2, FAd2, MI2, MI2, MI2, MI2, RE2, RE2, SOLd1, SOLd1, SOLd1, SOLd1, LA1, LA1, LA1, LA1, LAd1, LAd1, LAd1, LAd1,
    /* Mesure 19 */ SI1, SI1, SI1, SI1, RE2, RE2, RE2, RE2, MI2, MI2, FA2, FA2, FA2, FA2, FAd2, FAd2, FAd2, FAd2, LA2, LA2, LA2, LA2, FAd2, FAd2, FAd2, FAd2, LA2, LA2, LAd2, LAd2, LAd2, LAd2,
    /* Mesure 20 */ SI2, SI2, SI2, SI2, LA2, LA2, SI2, SI2, LA2, LA2, LA2, LA2, LAd2, LAd2, SI2, SI2, 0, 0, SOLd1, SOLd1, SOLd1, SOLd1, SOLd1|UEV, SOLd1, LA1, LA1, LA1|UEV, LA1, LAd1, LAd1, LAd1, LAd1
};
const uint32_t giornos_theme__piste1[640] = {
    /* Mesure 1 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 2 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 3 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 4 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 5 */ DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2,
    /* Mesure 6 */ DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2,
    /* Mesure 7 */ DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2,
    /* Mesure 8 */ DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, DOd2, DOd2, DOd2|UEV, DOd2, DO2, DO2, DO2, DO2, RE2, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, RE2|UEV, RE2, 0, 0,
    /* Mesure 9 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 10 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 11 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 12 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 13 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 14 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 15 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 16 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 17 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 18 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 19 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0,
    /* Mesure 20 */ DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0, DO2, DO2, DO2, DO2, FAd2, FAd2, 0, 0
};

musique giornos_theme () {
    musique m;

    m.BPM = 130;
    m.div_tempo = 8;
    m.nb_notes_piste = 640;
    m.nb_pistes = 2;
    m.pistes[0] = giornos_theme__piste0;
    m.pistes[1] = giornos_theme__piste1;
    m.pistes[2] = giornos_theme__piste0;
    m.pistes[3] = giornos_theme__piste0;
    m.pistes[4] = giornos_theme__piste0;
    m.pistes[5] = giornos_theme__piste0;
    m.pistes[6] = giornos_theme__piste0;
    m.pistes[7] = giornos_theme__piste0;

    return m;
}