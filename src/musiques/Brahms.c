

#include <stdint.h>
#include "../notes.h"
#include "Brahms.h"

/* Place occupée en mémoire: 3,1 ko */

const uint32_t Brahms__piste0[384] = {
    /* Mesure 1 */ LA4, LA4, LA4|UEV, LA4,
    /* Mesure 2 */ DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, LA4|UEV, LA4, LA4, LA4,
    /* Mesure 3 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, DO5, DO5,
    /* Mesure 4 */ FA5, FA5, FA5, FA5, MI5, MI5, MI5, MI5, MI5, MI5, RE5, RE5,
    /* Mesure 5 */ RE5|UEV, RE5, RE5, RE5, DO5, DO5, DO5, DO5, SOL4, SOL4, LA4, LA4,
    /* Mesure 6 */ LAd4, LAd4, LAd4, LAd4, SOL4, SOL4, SOL4, SOL4, SOL4|UEV, SOL4, LA4, LA4,
    /* Mesure 7 */ LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, SOL4, SOL4, LAd4, LAd4,
    /* Mesure 8 */ MI5, MI5, RE5, RE5, DO5, DO5, DO5, DO5, MI5, MI5, MI5, MI5,
    /* Mesure 9 */ FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA4, FA4, FA4|UEV, FA4,
    /* Mesure 10 */ FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA5, RE5, RE5, LAd4, LAd4,
    /* Mesure 11 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, FA4, FA4,
    /* Mesure 12 */ LAd4, LAd4, LAd4, LAd4, DO5, DO5, DO5, DO5, RE5, RE5, RE5, RE5,
    /* Mesure 13 */ LA4, LA4, DO5, DO5, DO5, DO5, DO5, DO5, FA4, FA4, FA4|UEV, FA4,
    /* Mesure 14 */ FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA5, RE5, RE5, LAd4, LAd4,
    /* Mesure 15 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, FA4, FA4,
    /* Mesure 16 */ LAd4, LAd4, DO5, LAd4, LA4, LA4, LA4, LA4, SOL4, SOL4, SOL4, SOL4,
    /* Mesure 17 */ FA4, FA4, FA4, FA4, FA4, FA4, FA4, FA4,
    /* Mesure 18 */ LA4, LA4, LA4|UEV, LA4,
    /* Mesure 19 */ DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, LA4|UEV, LA4, LA4, LA4,
    /* Mesure 20 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, DO5, DO5,
    /* Mesure 21 */ FA5, FA5, FA5, FA5, MI5, MI5, MI5, MI5, MI5, MI5, RE5, RE5,
    /* Mesure 22 */ RE5|UEV, RE5, RE5, RE5, DO5, DO5, DO5, DO5, SOL4, SOL4, LA4, LA4,
    /* Mesure 23 */ LAd4, LAd4, LAd4, LAd4, SOL4, SOL4, SOL4, SOL4, SOL4|UEV, SOL4, LA4, LA4,
    /* Mesure 24 */ LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, LAd4, SOL4, SOL4, LAd4, LAd4,
    /* Mesure 25 */ MI5, MI5, RE5, RE5, DO5, DO5, DO5, DO5, MI5, MI5, MI5, MI5,
    /* Mesure 26 */ FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA4, FA4, FA4|UEV, FA4,
    /* Mesure 27 */ FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA5, RE5, RE5, LAd4, LAd4,
    /* Mesure 28 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, FA4, FA4,
    /* Mesure 29 */ LAd4, LAd4, LAd4, LAd4, DO5, DO5, DO5, DO5, RE5, RE5, RE5, RE5,
    /* Mesure 30 */ LA4, LA4, DO5, DO5, DO5, DO5, DO5, DO5, FA4, FA4, FA4|UEV, FA4,
    /* Mesure 31 */ FA5, FA5, FA5, FA5, FA5, FA5, FA5, FA5, RE5, RE5, LAd4, LAd4,
    /* Mesure 32 */ DO5, DO5, DO5, DO5, DO5, DO5, DO5, DO5, LA4, LA4, FA4, FA4,
    /* Mesure 33 */ LAd4, LAd4, DO5, LAd4, LA4, LA4, LA4, LA4, SOL4, SOL4, SOL4, SOL4,
    /* Mesure 34 */ FA4, FA4, FA4, FA4, FA4, FA4, FA4, FA4
};
const uint32_t Brahms__piste1[384] = {
    /* Mesure 1 */ 0, 0, 0, 0,
    /* Mesure 2 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 3 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 4 */ FA3, FA3, FA3, FA3, LAd3, LAd3, LAd3, LAd3, RE4, RE4, RE4, RE4,
    /* Mesure 5 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 6 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 7 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 8 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 9 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 10 */ FA3, FA3, FA3, FA3, LAd3, LAd3, LAd3, LAd3, RE4, RE4, RE4, RE4,
    /* Mesure 11 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 12 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 13 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 14 */ FA3, FA3, FA3, FA3, LAd3, LAd3, LAd3, LAd3, RE4, RE4, RE4, RE4,
    /* Mesure 15 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 16 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 17 */ FA3, FA3, FA3, FA3, FA3, FA3, FA3, FA3,
    /* Mesure 18 */ 0, 0, 0, 0,
    /* Mesure 19 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 20 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 21 */ FA3, FA3, FA3, FA3, LAd3, LAd3, LAd3, LAd3, RE4, RE4, RE4, RE4,
    /* Mesure 22 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 23 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 24 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 25 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 26 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 27 */ FA3, FA3, FA3, FA3, LAd3, LAd3, LAd3, LAd3, RE4, RE4, RE4, RE4,
    /* Mesure 28 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 29 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 30 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 31 */ FA3, FA3, FA3, FA3, LAd3, LAd3, LAd3, LAd3, RE4, RE4, RE4, RE4,
    /* Mesure 32 */ FA3, FA3, FA3, FA3, LA3, LA3, LA3, LA3, DO4, DO4, DO4, DO4,
    /* Mesure 33 */ DO3, DO3, DO3, DO3, MI3, MI3, MI3, MI3, SOL3, SOL3, SOL3, SOL3,
    /* Mesure 34 */ FA3, FA3, FA3, FA3, FA3, FA3, FA3, FA3
};

musique Brahms () {
    musique m;

    m.BPM = 120;
    m.div_tempo = 4;
    m.nb_notes_piste = 384;
    m.nb_pistes = 2;
    m.pistes[0] = Brahms__piste0;
    m.pistes[1] = Brahms__piste1;
    m.pistes[2] = Brahms__piste0;
    m.pistes[3] = Brahms__piste0;
    m.pistes[4] = Brahms__piste0;
    m.pistes[5] = Brahms__piste0;
    m.pistes[6] = Brahms__piste0;
    m.pistes[7] = Brahms__piste0;

    return m;
}

