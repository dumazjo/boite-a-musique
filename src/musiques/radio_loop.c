#include <stdint.h>
#include "../notes.h"
#include "radio_loop.h"

/* Place occupée en mémoire: 4,2 ko */

const uint32_t radio_loop__piste0[528] = {
    /* Mesure 1 */ MI5, MI5, MI5, MI5, MI5, MI5, MI5, MI5, MI5, MI5, MI5, MI5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 2 */ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, SI5, SI5, SI5, SOLd5, SOLd5, SOLd5, FAd5, FAd5, FAd5, FAd5|UEV, FAd5, FAd5,
    /* Mesure 3 */ FAd5, FAd5, FAd5, FAd5, FAd5, FAd5, SOLd5, SOLd5, SOLd5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, SI5, SI5, SI5, SOLd5, SOLd5, SOLd5, FAd5, FAd5, FAd5, FAd5|UEV, FAd5, FAd5,
    /* Mesure 4 */ FAd5, FAd5, FAd5, 0, 0, 0, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, MI5, MI5, MI5, MI5, MI5, MI5, FAd5, FAd5, FAd5, SI4, SI4, SI4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 5 */ FAd5, FAd5, FAd5, FAd5, FAd5, FAd5, SOLd5, SOLd5, SOLd5, LA5, LA5, LA5, LA5, LA5, LA5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, REd5, REd5, REd5, REd5, REd5, REd5, MI5, MI5, MI5, FAd5, FAd5, FAd5, FAd5, FAd5, FAd5, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4,
    /* Mesure 6 */ SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, SI5, SI5, SI5, SOLd5, SOLd5, SOLd5, FAd5, FAd5, FAd5, FAd5|UEV, FAd5, FAd5,
    /* Mesure 7 */ FAd5, FAd5, FAd5, FAd5, FAd5, FAd5, SOLd5, SOLd5, SOLd5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, SI5, SI5, SI5, SOLd5, SOLd5, SOLd5, FAd5, FAd5, FAd5, FAd5|UEV, FAd5, FAd5,
    /* Mesure 8 */ FAd5, FAd5, FAd5, 0, 0, 0, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, MI5, MI5, MI5, MI5, MI5, MI5, FAd5, FAd5, FAd5, SI4, SI4, SI4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    /* Mesure 9 */ FAd5, FAd5, FAd5, FAd5, FAd5, FAd5, SOLd5, SOLd5, SOLd5, LA5, LA5, LA5, LA5, LA5, LA5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, SOLd5, REd5, REd5, REd5, REd5, REd5, REd5, MI5, MI5, MI5, FAd5, FAd5, FAd5, FAd5, FAd5, FAd5, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4,
    /* Mesure 10 */ DO5, DO5, DO5, DO5, DO5, DO5, 0, 0, 0, MI5, MI5, MI5, MI5, MI5, MI5, 0, 0, 0, SOL5, SOL5, SOL5, SOL5, SOL5, SOL5, 0, 0, 0, 0, 0, 0, SI5, SI5, SI5, DO6, DO6, DO6, SI5, SI5, SI5, SOL5, SOL5, SOL5, MI5, MI5, MI5, 0, 0, 0,
    /* Mesure 11 */ SI4, SI4, SI4, SI4, SI4, SI4, 0, 0, 0, REd5, REd5, REd5, REd5, REd5, REd5, 0, 0, 0, FAd5, FAd5, FAd5, FAd5, FAd5, FAd5, 0, 0, 0, 0, 0, 0, LA5, LA5, LA5, SI5, SI5, SI5, LA5, LA5, LA5, FAd5, FAd5, FAd5, REd5, REd5, REd5, REd5, REd5, REd5
};
const uint32_t radio_loop__piste1[528] = {
    /* Mesure 1 */ SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, 0, 0, 0, 0, 0, 0, LA4, LA4, LA4, LA4, LA4, LA4, 0, 0, 0, 0, 0, 0, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5|UEV, DOd5, DOd5, SI4, SI4, SI4, 0, 0, 0,
    /* Mesure 2 */ SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, 0, 0, 0, 0, 0, 0, LA4, LA4, LA4, LA4, LA4, LA4, 0, 0, 0, 0, 0, 0, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5|UEV, DOd5, DOd5, SI4, SI4, SI4, 0, 0, 0,
    /* Mesure 3 */ SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, 0, 0, 0, 0, 0, 0, LA4, LA4, LA4, LA4, LA4, LA4, 0, 0, 0, 0, 0, 0, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5|UEV, DOd5, DOd5, SI4, SI4, SI4, 0, 0, 0,
    /* Mesure 4 */ SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, LA4, LA4, LA4, LA4, LA4, LA4, LA4, LA4, LA4, LA4, LA4, LA4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5|UEV, DOd5, DOd5, SI4, SI4, SI4, 0, 0, 0,
    /* Mesure 5 */ FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, LA4, LA4, LA4, LA4, LA4, LA4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4,
    /* Mesure 6 */ MI4, MI4, MI4, MI4, MI4, MI4, 0, 0, 0, 0, 0, 0, LA4, LA4, LA4, LA4, LA4, LA4, 0, 0, 0, 0, 0, 0, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5|UEV, DOd5, DOd5, SI4, SI4, SI4, 0, 0, 0,
    /* Mesure 7 */ SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, 0, 0, 0, 0, 0, 0, LA4, LA4, LA4, LA4, LA4, LA4, 0, 0, 0, 0, 0, 0, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5|UEV, DOd5, DOd5, SI4, SI4, SI4, 0, 0, 0,
    /* Mesure 8 */ SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, 0, 0, 0, 0, 0, 0, LA4, LA4, LA4, LA4, LA4, LA4, 0, 0, 0, 0, 0, 0, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, SI4, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5, DOd5|UEV, DOd5, DOd5, SI4, SI4, SI4, 0, 0, 0,
    /* Mesure 9 */ FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, LA4, LA4, LA4, LA4, LA4, LA4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, REd4, FAd4, FAd4, FAd4, FAd4, FAd4, FAd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4, SOLd4,
    /* Mesure 10 */ LA4, LA4, LA4, LA4, LA4, LA4, 0, 0, 0, DO5, DO5, DO5, DO5, DO5, DO5, 0, 0, 0, MI5, MI5, MI5, MI5, MI5, MI5, 0, 0, 0, 0, 0, 0, SI4, SI4, SI4, DO5, DO5, DO5, SI4, SI4, SI4, SOL4, SOL4, SOL4, MI4, MI4, MI4, 0, 0, 0,
    /* Mesure 11 */ SOL4, SOL4, SOL4, SOL4, SOL4, SOL4, 0, 0, 0, SI4, SI4, SI4, SI4, SI4, SI4, 0, 0, 0, REd5, REd5, REd5, REd5, REd5, REd5, 0, 0, 0, 0, 0, 0, LA4, LA4, LA4, SI4, SI4, SI4, LA4, LA4, LA4, FAd4, FAd4, FAd4, REd4, REd4, REd4, REd4, REd4, REd4
};

musique radio_loop () {
    musique m;

    m.BPM = 120;
    m.div_tempo = 12;
    m.nb_notes_piste = 528;
    m.nb_pistes = 2;
    m.pistes[0] = radio_loop__piste0;
    m.pistes[1] = radio_loop__piste1;
    m.pistes[2] = radio_loop__piste0;
    m.pistes[3] = radio_loop__piste0;
    m.pistes[4] = radio_loop__piste0;
    m.pistes[5] = radio_loop__piste0;
    m.pistes[6] = radio_loop__piste0;
    m.pistes[7] = radio_loop__piste0;

    return m;
}