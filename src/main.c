#include <stdio.h>
#include <math.h>
#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/init.h"
#include "sys/clock.h"
#include "systick.h"
#include "adc.h"

#include "init_func.h"
#include "mon_utils.h"
#include "notes.h"
#include "buzzer.h"
#include "io.h"
#include "button.h"

#include "musiques/cara_mia_addio.h"
#include "musiques/Megalovania.h"
#include "musiques/bloody_stream.h"
#include "musiques/scooby_doo.h"
#include "musiques/au_clair_de_la_lune.h"
#include "musiques/Brahms.h"
#include "musiques/radio_loop.h"
#include "musiques/giornos_theme.h"

/**
 * Drapeaux
*/
volatile uint32_t drapeau = 1;
volatile uint32_t ihm_maj = 0;
volatile uint32_t lecture = 0;

/**
 * Paramètres Timer
 */
volatile uint32_t cpt = 0;
volatile uint32_t freq = 20000;
volatile uint32_t volume = 0;

/**
 * Paramètres Systick
 */
volatile uint32_t tempo = 120; // bpm
volatile uint32_t temps = 125000; // µs

/* Paramètres */
volatile uint8_t piste = 0;
volatile uint8_t choix_musique = 0;
volatile uint8_t mode_suiveur = 0;
volatile uint32_t n = 0;

/*zoulies lumières*/
volatile int a=0;
volatile int MemFreq=0;

/**
 * Lis les entrées de l'utilisateur, 
 * met à jour les paramètres en accord avec iceux
 */
void get_IHM ();

/*Fonction d'interruption du SysTick*/
void __attribute__((interrupt)) SysTick_Handler() {
    cpt ++;
    if(cpt >= temps) {
        drapeau = 1;
        cpt = 0;
        GPIOA.ODR|=(1);
    }else{GPIOA.ODR&=~1;}
}

/*Fonction d'interruption du bouton*/
void __attribute__((interrupt)) EXTI9_5_Handler () {

    EXTI.PR |= (1 << 8);
    
    if (!button_poll()) return;

    if (mode_suiveur && !lecture) {
        piste++;
        if (piste == 4) piste = 0;
        ihm_maj = 1;
    } else {
        lecture = 1;
    }

}

int main() {

    /*initialisation des GPIO et des ports*/
    enable_GPIOA();
    enable_GPIOB();
    enable_ADC1();

    init_potentiometre();
    led_init();  
    init_buzzer();
    button_init();
    button_irq_init();

    ADC_set_sampling_time(&ADC1, 8, 7);
    ADC_set_prediv(3);
    ADC_init_single_channel(&ADC1, 8);

    get_IHM();
    // EXTI.IMR |= (0b111<<4); //masque des des interruptions sur GPIO 5 7 8 9

    ihm_maj = 1;    
    drapeau = 0;

    musique musiques[8];
    musiques[0] = au_clair_de_la_lune();
    musiques[1] = Brahms();
    musiques[2] = bloody_stream();
    musiques[3] = giornos_theme(); 
    musiques[4] = scooby_doo();
    musiques[5] = Megalovania(); 
    musiques[6] = cara_mia_addio();
    musiques[7] = radio_loop(); 

    /*récupération de la musique à jouer*/
    musique m = musiques[choix_musique & 7];
    uint32_t la_note;

    // Config Systick
    if (!mode_suiveur) {
        while (!lecture) ;
        SysTick_init(1000);
            GPIOA.MODER &= ~( 0b11);
            GPIOA.MODER |= 1;

    }else
    {
        SysTick_deinit();
        GPIOA.MODER &= ~( 0b11);
    	GPIOA.PUPDR = (GPIOA.PUPDR & ~3) | 2;
        init_PA0_irq();
    }
 

    /*récupération des données de tempo et de temps des notes*/
    tempo = m.BPM;
    temps = 60000 / (m.div_tempo * m.BPM);

    printf("%lu", temps);
    
    /*initialisation du volume et du pwm à 0*/
    set_frequence(0);
    set_volume(volume);
    force_UEV();

    for (;;) {  //boucle infinie

        while (!drapeau ) {
            get_IHM();
            set_volume(volume);
            printf("%lu\r", volume);
            if (ihm_maj) {
                set4led(1 << piste);
                ihm_maj = 0;
            }
        };
        

        lecture = 1;

        la_note = m.pistes[piste][n] & ~UEV;
        
        /*Section qui fait des zoulies lumières*/
        if (la_note <= 20){
   		    settriled(0);
   		}
   		 else if( MemFreq != la_note){
   		    settriled((la_note % 6)+1);//A peu près aleatoire sinon %6 + 1;
    	}
    	if (a>=7){
   			a=0;
   		}
   		a++;
    
	    MemFreq = la_note;   
        
        /*Section qui joue la note*/
        set_frequence(la_note);
        if (m.pistes[piste][n] & UEV) force_UEV();         // Crée une micro coupure sonore
    
        cpt = 0;

        drapeau = 0;

        n = (n + 1);
        if (n >= m.nb_notes_piste) n = 0;

    }

return 0;
}

/*Fonction qui permet d'obtenir les bits du cadran et de les mettre dans mode_suiveur*/
void get_IHM () {

    volume = 4096 - ADC_convert_single_channel(&ADC1);
    uint32_t dip = (GPIOB.IDR >> 3) & 0xf;
    choix_musique = dip & 7; // Codé sur 3 bits
    mode_suiveur = dip >> 3;
}

/*changement*/


void __attribute__ ((interrupt)) EXTI0_Handler() {

    printf("Test\n\r");
	drapeau = 1;

    EXTI.PR |= (1 << 0);
}
 