#include "mon_utils.h"
#include "sys/devices.h"

void led_init () {
    GPIOA.MODER = (GPIOA.MODER & ~(0x3FFF<<8)) | (0x1555<<8);
    GPIOA.OTYPER &= ~(0x7F<<4);
    GPIOA.OSPEEDR |= (0x3FFF<<8);
}

void set4led (uint8_t led) {
    GPIOA.ODR = (GPIOA.ODR & ~(0xF0)) | ((led & 0xF) << 4);
}

uint8_t get4led () {
    return (GPIOA.ODR & 0xF0) >> 4;
}

char _getch () {
    while (!(USART2.SR & 0x20)) ;
    return USART2.DR & 0x7F;
}

void _putch (char c) {
    while (!(USART2.SR & 0x80)) ;
    USART2.DR = c & 0xFF;
}

void led_tri_set (uint8_t status) {
    GPIOA.ODR = (GPIOA.ODR & ~(7<<8)) | (1<<(8+status));    
}

uint8_t led_tri_get () {
    return (GPIOA.ODR >> 8) & 7;
}

void led_tri_off () {
     GPIOA.ODR &= ~(7<<8); 
}

void init_potentiometre () {
    GPIOB.MODER |= 3; // PB0 en Analog
}
