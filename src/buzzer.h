#ifndef __BAM__BUZZER__
#define __BAM__BUZZER__

#include <stdint.h>

#define MAX_FREQ 1600000

/**
 * Paramètre PB9 et Timer2
*/
void init_buzzer ();

/**
 * Force l'actualisation de Timer2
*/
void force_UEV ();

/**
 *  Définit une note et son volume 
 * 
 *  @param frequence fréquence de la note
 *  @param volume pourcentage du volume du son (entre 0 et 4096)
 */
void set_frequence (uint32_t frequence);

void set_volume(uint32_t volume);

#endif