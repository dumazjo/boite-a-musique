#ifndef __MON_UTILS__
#define __MON_UTILS__

#include <stdint.h>

void led_init();

void set4led(uint8_t led);

uint8_t get4led();

char _getch ();

void _putch (char c);

void led_tri_set (uint8_t status);

uint8_t led_tri_get();

 void led_tri_off (); 

void init_potentiometre ();

#endif
