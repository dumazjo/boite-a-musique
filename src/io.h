#ifndef __BAM__IO__
#define __BAM__IO__

#include <stdint.h>

void affiche_stats ();
void affiche_freq ();
void affiche_timer ();
uint32_t get_int (char *titre, uint8_t max_chr);

#endif