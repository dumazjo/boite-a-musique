#include <stdio.h>
#include <stdint.h>
#include "io.h"
#include "notes.h"

#include "sys/cm4.h"
#include "sys/devices.h"
#include "sys/init.h"
#include "sys/clock.h"
#include "mon_utils.h"

extern volatile uint32_t freq;
extern volatile uint32_t volume;

void affiche_stats () {

    printf("\e[2J\e[1;1H\e[?25l\r\n\e[37;44m");
    printf("\e[2C +----------------------------------+ \r\e[1B");
    printf("\e[2C | *** Welcome to Nucleo F446 ! *** | \r\e[1B");
    printf("\e[2C |    %08lx-%08lx-%08lx    | \r\e[1B",U_ID[0],U_ID[1],U_ID[2]);
    printf("\e[2C | SYSCLK  = %9lu Hz           | \r\e[1B",get_SYSCLK());
    printf("\e[2C | AHBCLK  = %9lu Hz           | \r\e[1B",get_AHBCLK());
    printf("\e[2C | APB1CLK = %9lu Hz           | \r\e[1B",get_APB1CLK());
    printf("\e[2C | APB2CLK = %9lu Hz           | \r\e[1B",get_APB2CLK());
    printf("\e[2C +----------------------------------+ \r\e[1B\e[0m\n");

}

void affiche_freq () {

    printf("\e[H\e[1B\e[42C\e[100;37m"); // Déplacement sur le terminal

    printf(" +---------------------+ \e[25D\e[1B");
    printf(" | Fréquence: \e[92m%5lu\e[37m Hz | \e[25D\e[1B", freq);
    printf(" | Volume:    \e[92m%5lu\e[37m    | \e[25D\e[1B", (TIM2.ARR * volume) / 8192);
    printf(" +---------------------+ \r\e[1B\e[0m\n");

}

void affiche_timer () {

    printf("\e[H\e[9B\e[2C\e[100;37m"); // Déplacement sur le terminal

    printf("\r\e[1B\e[2C +------------------+ ");
    printf("\r\e[1B\e[2C | PSC  = %9lu | ", TIM2.PSC);
    printf("\r\e[1B\e[2C | ARR  = %9lu | ", TIM2.ARR);
    printf("\r\e[1B\e[2C | CCR2 = %9lu | ", TIM2.CCR2);
    printf("\r\e[1B\e[2C +------------------+ ");

}

uint32_t get_int (char *titre, uint8_t max_chr) {

    char c = 0;
    uint32_t note = 0;
    uint8_t nb_chr = 0;

    printf("\e[H\e[2C\e[10B\e[33m%s > \e[36m", titre);

    for (c = _getch(); c != '\r' && nb_chr < max_chr; c = _getch()) {
        
        if (c < '0' || c > '9') continue;
        nb_chr++;
        _putch(c);
        note = note * 10 + (c - '0');
        
    }
    
    printf("\e[0m\e[2K");

    return note;

}
