#include <stdint.h>
#include "sys/devices.h"
#include "init_func.h"

void setled(uint8_t led)
{
	GPIOA.ODR=GPIOA.ODR & 0xF0F;
	GPIOA.ODR =GPIOA.ODR | (led<<4);
}

void enableGPIOA()
{
	
	RCC.AHB1ENR=((RCC.AHB1ENR)| 0x01);
	GPIOA.MODER=GPIOA.MODER &~ (0x3FFF<<8);
	GPIOA.MODER=GPIOA.MODER | (0x155500);
}


void settriled(uint8_t led)
{
	GPIOA.ODR=GPIOA.ODR &~ (7<<8);
	GPIOA.ODR =GPIOA.ODR | (led<<(8));
	
}

/*uint8_t _getch()
{
	while ((USART2.SR & 0x20)==0)
	{
	}
	return (USART2.DR) & 0x7F;

	}

char _putchar (uint8_t cara)
{
	while ((USART2.SR & 0x80)==0)
	;
	USART2.DR = (cara & 0x7F);
}*/
