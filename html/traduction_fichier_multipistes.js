const patterns = {
    header: /^>(\W+)?(?<nom>\w+)\ (?<bpm>\d+)\ (?<div>\d+)\ (?<nb_pistes>\d+)\ (?<mode>\w+)?(\W+)?$/m,
    commentaire: /\/\/.*$/mg,
    commentaire_multiligne: /\/\*.+\*\//mgs,
    mesure: /(\W+)?(?<mesure>\d+)(\W+){1,}(?<piste>\d+)(\W+)\((?<notes>[^\)]*)\)/mg,
    note: /(?<uev>!)?(?<note>[0-9A-Zxd]+)?:(?<duree>\w+)?/g,
}

/**
 * Transforme le fichier .txt en .c et .h correspondants
 * @param {string} fichier Les notes à traduire au format utf-8
 * @returns {{ c: string, h: string, nom: string }} Les fichiers C et H
 */
function parseFile (fichier, ignore_nb_diff = false) {

    // On récupère l'entête
    let header = fichier.match(patterns.header);
    if (!header) throw "Fichier: Entête manquante";
    
    // On la décompose
    let { nom, bpm, div, nb_pistes, mode } = header.groups;

    // On prépare les pistes
    let pistes = Array(nb_pistes);
    for (let i = 0; i < nb_pistes; i++) pistes[i] = Array();

    // On enlève les commentaires
    fichier = fichier.replace(patterns.commentaire, "").replace(patterns.commentaire_multiligne, ""); 

    // On récupère les mesures
    let mesures = [...fichier.matchAll(patterns.mesure)];
    for (let m of mesures) {
        let { mesure, piste, notes } = m.groups;
        let tab_notes = [ ...notes.matchAll(patterns.note) ];
        tab_notes = [...tab_notes.map((n, i) => {
            let { uev, note, duree } = n.groups;
            
            if (!note) throw `Syntaxe: Mesure ${mesure}, piste ${piste}, note ${i + 1}: Note manquante`;
            
            duree = parseInt(duree) || 0;
            if (!duree) throw `Syntaxe: Mesure ${mesure}, piste ${piste}, note ${i + 1}: Durée manquante`;

            if (mode == "MIDI")
                note = convertirMIDI(parseInt(note) || 0);
            else note = note == "x" ? "0" : note;
            
            let notes = Array(duree).fill(note);
            
            if (uev) notes[0] += '|UEV';

            return notes;            
        })].flat();
        pistes[piste][mesure - 1] = (tab_notes);
    }

    let compte = piste => piste.length ? piste.map( e => e.length ).reduce( (p, c, i, t) => p + c ) : 0;

    let nb_toute_piste = pistes.map(compte);
    let nb = nb_toute_piste[0];

    let non_ok = [];
    for (let i in nb_toute_piste) 
        if (nb_toute_piste[i] != nb) non_ok.push(i);

    if (non_ok.length && !ignore_nb_diff) 
        throw `Longueur: Les pistes ${non_ok} et 0 ne sont pas de même taille (0: ${nb} | ${non_ok.map(e => `${e}: ${nb_toute_piste[e]}`).join(" | ")})`;

    let c = fichier_C(nom, bpm, div, nb, pistes), 
        h = fichier_H(nom);

    return { c, h, nom };

}

/**
 * Crée le contenu du fichier H pour le fichier C
 * @param {string} nom 
 */
function fichier_H (nom) {
    return `#ifndef __BAM__${nom.toLocaleUpperCase()}__
#define __BAM__${nom.toLocaleUpperCase()}__

#include "../notes.h"

/**
 * Retourne l'objet musique "${nom}"
 */
musique ${nom}();

#endif`
}

/**
 * Crée le contenu du fichier C contenant les notes
 * 
 * @param {string} nom 
 * @param {string} bpm
 * @param {string} div 
 * @param {number} nb_notes 
 * @param {string[][][]} pistes 
 */
function fichier_C (nom, bpm, div, nb_notes, pistes) {
    return `#include <stdint.h>
#include "../notes.h"
#include "${nom}.h"

/* Place occupée en mémoire: ${new Intl.NumberFormat("fr-FR", { notation: "compact", unit: "byte", style: "unit", unitDisplay: "narrow" }).format(pistes.length * nb_notes * 4)} */

${pistes.map(
    (p, i) => `const uint32_t ${nom}__piste${i}[${nb_notes}] = {\n    ${ p.map((e, k) => `/* Mesure ${k + 1} */ ${e.join(", ")}`).join(",\n    ") }\n};`
).join("\n")}

musique ${nom} () {
    musique m;

    m.BPM = ${bpm};
    m.div_tempo = ${div};
    m.nb_notes_piste = ${nb_notes};
    m.nb_pistes = ${pistes.length};
    ${pistes.map((_, i) => `m.pistes[${i}] = ${nom}__piste${i};`).join("\n    ")}
    ${Array(8 - pistes.length).fill(0).map((_, i) => `m.pistes[${i + pistes.length}] = ${nom}__piste0;`).join("\n    ")}

    return m;
}`
}

const noms = [ 'C', 'Cd', 'D', 'Dd', 'E', 'F', 'Fd', 'G', 'Gd', 'A', 'Ad', 'B' ];
const noms_fr = [ 'DO', 'DOd', 'RE', 'REd', 'MI', 'FA', 'FAd', 'SOL', 'SOLd', 'LA', 'LAd', 'SI' ];

/**
 * Convertis une note midi en note [Anglaise/Française]
 * @param {number} n 
 * @param {boolean} fr 
 * @returns 
 */
function convertirMIDI (n, fr = true) {
    if (!n) return "0";

    let note = n % 12;
    let octave = ~~(n / 12) - 1;
    return `${ (fr ? noms_fr : noms)[note] }${octave}`;
}

// MAIN - Pour NodeJS
if (typeof window === 'undefined') {

    const { argv, exit } = require("process");
    const { readFileSync, writeFileSync, existsSync } = require("fs");

    if (argv.length < 3) {
        console.error(`node ${argv[1]} <fichier.txt>`);
        exit(1);
    }
    
    if (!existsSync(argv[2])) {
        console.error(`Fichier non disponible`);
        exit(2);
    }

    const notes = readFileSync(argv[2] , "utf-8");

    try {
        const { c, h, nom } = parseFile(notes);

        writeFileSync(nom + ".h", h, "utf-8");
        writeFileSync(nom + ".c", c, "utf-8");
    } catch (e) {
        console.log(e);
        exit(3)
    }
   

} else {
    console.log("Convertisseur OK !");
}