# Les 12 nuits

Ce répertoire contient le code source de la boîte à musique "Les 12 nuits",
dans le cadre du cours de Microcontrôleurs de la filière IESE.

Nous utilisons une carte STM32 F446 munies d'une carte fille munie 
d'un buzzer, de deux boutons poussoirs, de 4 interrupteurs, d'une 
barre de 4 LEDs, d'une LED RGB, et d'un potentiomètre.

Il est possible de relier plusieurs boîtes à musique ensemble pour 
jouer des musiques avec plusieurs pistes sonores en même temps.

## Manuel d'utilisation

Clonez ce répertoire et uploadez le code source à l'aide de la commande `make load`.

### Mode classique
Pour choisir la musique, utilisez les **interrupteurs 2, 3 et 4** pour coder en
binaire un nombre entre 0 et 7 (l'interrupteur 4 étant le bit de poids faible). 
Puis appuyez sur le bouton **reset** pour valider le changement.

Pour changer le volume, utilisez simplement le potentiomètre.

Pour lancer une musique, appuyez sur le **bouton noir**.

### Mode suiveur

Pour utiliser le mode suiveur, reliez les pins GND de chaques cartes entre-eux
ainsi que les pins A0 (PA0) entre-eux. 
Ensuite, basculez l'**interrupteur 1** des cartes devant être en mode suiveur, 
codez la musique voulue avec **les interrupteurs 2, 3 et 4**
et appuyez sur **reset** pour valider le changement.

Avant de lancer la lecture sur la carte maître, veillez à choisir la piste 
devant être lue par chaque suiveur avec le **bouton noir**, 
vous aurez comme indication de votre selection une des 4 LEDs de la **barre de 4 LEDs**.

Vous pouvez appuyer sur le **bouton noir** de la carte maître pour lancer la musique.

### Ecrire de la musique

Pour écrire de la musique, référez-vous à l'outil fourni dans le dossier `html`. 
Ouvrez la page `outil.html` dans votre navigateur.